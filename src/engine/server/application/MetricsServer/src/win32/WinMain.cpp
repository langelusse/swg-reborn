#include "sharedFoundation/FirstSharedFoundation.h"

#include "ConfigMetricsServer.h"
#include "MetricsServer.h"

#include "sharedCompression/SetupSharedCompression.h"
#include "sharedDebug/SetupSharedDebug.h"
#include "sharedFile/SetupSharedFile.h"
#include "sharedFoundation/Os.h"
#include "sharedFoundation/SetupSharedFoundation.h"
#include "sharedNetwork/NetworkHandler.h"
#include "sharedNetwork/SetupSharedNetwork.h"
#include "sharedNetworkMessages/SetupSharedNetworkMessages.h"
#include "sharedRandom/SetupSharedRandom.h"
#include "sharedThread/SetupSharedThread.h"


int main(int argc, char ** argv)
{
	SetupSharedThread::install();
	SetupSharedDebug::install(1024);

	//-- setup foundation
	SetupSharedFoundation::Data setupFoundationData(SetupSharedFoundation::Data::D_game);

	setupFoundationData.argc = argc;
	setupFoundationData.argv = argv;
	setupFoundationData.createWindow = false;
	setupFoundationData.clockUsesSleep = true;
	SetupSharedFoundation::install(setupFoundationData);

	SetupSharedCompression::install();
	SetupSharedFile::install(false);

	SetupSharedNetwork::SetupData  networkSetupData;
	SetupSharedNetwork::getDefaultServerSetupData(networkSetupData);
	SetupSharedNetwork::install(networkSetupData);
	SetupSharedRandom::install(int(time(NULL)));

	NetworkHandler::install();

	//setup the server
	ConfigMetricsServer::install();

	MetricsServer::install();

	//-- run server
	SetupSharedFoundation::callbackWithExceptionHandling(MetricsServer::run);

	MetricsServer::remove();
	NetworkHandler::remove();
	SetupSharedFoundation::remove();

	return 0;
}

//-----------------------------------------------------------------------
