for file in $(find ./dsrc -name "*.tab" | cut -sd / -f 2-)
do
	# Build output path (replace dsrc with data and tab to iff)
	outputfile="${file/.tab/.iff}"
	outputfile="${outputfile/dsrc/data}"
	
	# Make sure directory exists
	dir=$(dirname "${outputfile}")
	mkdir -p $dir 

	# build datatable file
	tools/DataTableTool -i $file -o $outputfile
done 