@echo off
setlocal EnableDelayedExpansion EnableExtensions

for /R "dsrc/sku.0/sys.server/compiled/game" %%G in (*.java) do (
    set "ifile=%%G"
    set ofile=!ifile:dsrc=data!
	if not exist "data" ( cmd /c md "data/sku.0/sys.server/compiled/game" )
    set destination="data/sku.0/sys.server/compiled/game"
    set sourcepath="dsrc/sku.0/sys.server/compiled/game"

    call :dir_from_path filepath !ofile!

    echo !ifile!
    
    if not exist !filepath! ( cmd /c md "!filepath!" )

    cmd /c javac -classpath "!destination!" -d "!destination!" -sourcepath "!sourcepath!" -g -deprecation "!ifile!"
)

goto :eof

:dir_from_path <resultVar> <pathVar>
(
    set "%~1=%~d2%~p2"
    exit /b
)

:eof
endlocal