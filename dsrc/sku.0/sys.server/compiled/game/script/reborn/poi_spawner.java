package script.reborn;

import script.*;
import script.base_class.*;
import script.combat_engine.*;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Vector;
import script.base_script;

import script.ai.ai;
import script.library.ai_lib;
import script.library.utils;
import script.library.create;
import script.library.trial;
import script.library.space_dungeon;
import script.library.instance;
import script.library.locations;
import script.library.spawning;

public class poi_spawner extends script.base_script
{
	public poi_spawner()
	{
	}
	public int OnHearSpeech(obj_id self, obj_id objSpeaker, String strText) throws InterruptedException
	{
		if (isGod(objSpeaker))
		{
			if (strText.equals("respawn"))
			{
				for(int i = 0; i < 1024; i++)
				{
					if(hasObjVar(self, "NPC_" + i))
					{
						obj_id npc = getObjIdObjVar(self, "NPC_" + i);
						if (!isIdValid(npc) || !exists(npc)) continue;
						
						kill(npc);
						destroyObject(npc);
						
						removeObjVar(self, "NPC_" + i);
					}
				}
				spawnNPCs(self);
				return SCRIPT_CONTINUE;
			}
		}
		return SCRIPT_CONTINUE;
	}
	public int OnAttach(obj_id self) throws InterruptedException
	{
		requestPreloadCompleteTrigger(self);
		return SCRIPT_CONTINUE;
	}
	
	public int OnInitialize(obj_id self) throws InterruptedException
	{
		requestPreloadCompleteTrigger(self);
		return SCRIPT_CONTINUE;
	}
	
	public int OnPreloadComplete(obj_id self) throws InterruptedException
	{
		spawnNPCs(self);
		return SCRIPT_CONTINUE;
	}
	
	public void spawnNPCs(obj_id self) throws InterruptedException
	{
		location localLocation = getLocation(self);
		
		String datatable = "datatables/spawning/poi_spawner/" + localLocation.area + ".iff";
		setObjVar(self, "datatable", datatable);
		
		for(int i = 0; i < dataTableGetNumRows(datatable); i++)
		{
			// Get location information
			String planet = dataTableGetString (datatable, i, "PLANET");
			
			int _containerId = dataTableGetInt (datatable, i, "BUILDING");
			obj_id containerId = utils.stringToObjId("" + _containerId);
			
			float x = dataTableGetFloat (datatable, i, "LOC_X");
			float y = dataTableGetFloat (datatable, i, "LOC_Y");
			float z = dataTableGetFloat (datatable, i, "LOC_Z");
			
			// Get spawn information
			String type = dataTableGetString (datatable, i, "TYPE");
			String scripts = dataTableGetString (datatable, i, "SCRIPTS");
			
			// Determine our spawn point
			location spawnPoint = null;
			
			// Check to see if location is inside a building
			if( _containerId > 0)
			{
				if(isIdValid(containerId))
				{
					try
					{
						debugSpeakMsg(self, "has container id: " + _containerId);
						
						// it's a cell, we have local coordinates
						if(getTemplateName(containerId).equals("object/cell/cell.iff"))
						{
							spawnPoint = new location (x, y, z, planet, containerId);
						}
						// it's a building and we have world coordinates
						else
						{
							spawnPoint = getLocationForWorldCoordinate(x, y, z);	
						}
					}
					catch(Exception ex)
					{
						CustomerServiceLog("Persistence.NPCSpawner", "couldn't find container: " + _containerId);
						continue;
					}
				}
				else continue;
			}
			
			if(spawnPoint == null)
			{
				if(_containerId == 0)
					spawnPoint = new location(x, y, z, planet);
				else 
					continue;
			}
			
			if(scripts.length() < 3)
			{
				obj_id spawner = createObject("object/tangible/ground_spawning/area_spawner.iff", spawnPoint);
				setObjVar(spawner, "fltMaxSpawnTime", dataTableGetFloat(datatable, i, "DELAY"));
				setObjVar(spawner, "fltMinSpawnTime", dataTableGetFloat(datatable, i, "DELAY"));
				setObjVar(spawner, "fltRadius", dataTableGetFloat(datatable, i, "RADIUS"));
				
				setObjVar(spawner, "intDefaultBehavior", dataTableGetInt(datatable, i, "SENTINEL"));
				setObjVar(spawner, "intGoodLocationSpawner", 0);
				setObjVar(spawner, "intSpawnCount", dataTableGetInt(datatable, i, "POP"));
				setObjVar(spawner, "intSpawnSystem", 1);
				
				setObjVar(spawner, "strName", type);
				setObjVar(spawner, "strSpawns", type);
				setObjVar(spawner, "strSpawnerType", "area");
				
				attachScript(spawner, "systems.spawning.spawner_area");
				
				setObjVar(self, "NPC_" + i, spawner);
			}
			else
			{
				// Calculate level
				int level = -1;
				if(dataTableGetInt(datatable, i, "PLANET_LEVEL") == 1)
					level = getRandomPlanetCreatureLevel(self, type, spawnPoint);
				
				// Spawn our creature
				obj_id creature = create.object (type, spawnPoint, level);
				setObjVar(self, "NPC_" + i, creature);
				
				// Sentinel (non-moving)
				if(dataTableGetInt(datatable, i, "SENTINEL") == 1)
					setCreatureObjVars(creature, "int:ai.defaultCalmBehavior=1");
				
				// Attach scripts
				if (scripts != null && !scripts.equals(""))
				{
					String[] scriptArray = split(scripts, ',');
					for (int j = 0; j < scriptArray.length; j++)
					{
						attachScript(creature, scriptArray[j]);
					}
				}
			}
		}
	}
	
	public int getRandomPlanetCreatureLevel(obj_id spawner, String npcType, location here) throws InterruptedException
    {
        int minLevel = locations.getMinDifficultyForPlanet(here.area);
        int maxLevel = locations.getMaxDifficultyForPlanet(here.area);
        utils.setScriptVar(spawner, "testing_planetMinLevel", minLevel);
        utils.setScriptVar(spawner, "testing_planetMaxLevel", maxLevel);
        utils.setScriptVar(spawner, "testing_here.area", here.area);
        int level = minLevel;
        dictionary creatureDict = utils.dataTableGetRow(create.CREATURE_TABLE, npcType);
        if (creatureDict != null)
        {
            int baseLevel = creatureDict.getInt("BaseLevel");
            level = baseLevel;
            if (minLevel > 0 && maxLevel > 0)
            {
                if (level < minLevel || level > maxLevel)
                {
                    if (level < minLevel)
                    {
                        level = minLevel;
                    }
                    else if (level > maxLevel)
                    {
                        level = maxLevel;
                    }
                    CustomerServiceLog("interior_spawner_level", "Spawner " + spawner + " tried to spawn a creature whose base level from creatures.tab (" + baseLevel + ") was outside of the planet's range (" + minLevel + " to " + maxLevel + ").");
                }
            }
        }
        if (level < 1)
        {
            level = 1;
        }
        return level;
    }
	
	public void setCreatureObjVars(obj_id creature, String objVarList) throws InterruptedException
    {
        if (objVarList == null || objVarList.equals(""))
        {
            return;
        }
        String[] pairs = split(objVarList, ',');
        for (int i = 0; i < pairs.length; i++)
        {
            String[] objVarToSet = split(pairs[i], '=');
            String objVarValue = objVarToSet[1];
            String[] objVarNameAndType = split(objVarToSet[0], ':');
            String objVarType = objVarNameAndType[0];
            String objVarName = objVarNameAndType[1];
            if (objVarType.equals("string"))
            {
                setObjVar(creature, objVarName, objVarValue);
            }
            else if (objVarType.equals("int"))
            {
                setObjVar(creature, objVarName, utils.stringToInt(objVarValue));
            }
            else if (objVarType.equals("float"))
            {
                setObjVar(creature, objVarName, utils.stringToFloat(objVarValue));
            }
            else if (objVarType.equals("boolean") || objVarType.equals("bool"))
            {
                setObjVar(creature, objVarName, utils.stringToInt(objVarValue));
            }
            else 
            {
                setObjVar(creature, objVarName, objVarValue);
            }
        }
    }
}